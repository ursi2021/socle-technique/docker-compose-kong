# Docker-Compose-kong

This repo contains a dockeer-compose.yml and a kong-init.sh script that registers the apps with theirs ports and paths. 


### Prerequisites

To run the docker-compose.yml you will need to have the following packages installed :

- docker
- docker-compose


### Installing / Running

To install the project follow those steps :

- Clone the repository

- Go in the repository 
```bash
cd [repository_name]
```

- Run the docker-compose.yml file
```bash
sudo docker-compose up -d
```

- Init the kong app
```bash
./kong-init.sh
```

## Contributing

Please keep in mind that master branch need to stay stable release or be use only for minor bug fix


## Versioning


## Authors

* **Equipe Socle technique URSI SIGL2021** - *Initial work* 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details